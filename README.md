Pebble Watchface: Vitality Inspired
===


Preview
---

Description  | Basalt / Time                           | Diorite / Pebble 2
------------ | ---                                     | ---
12 Hour View | ![Basalt 12 Hour][Basalt 12 Hour]       | ![Diorite 12 Hour][Diorite 12 Hour]
24 Hour View | ![Basalt 24 Hour][Basalt 24 Hour]       | ![Diorite 24 Hour][Diorite 24 Hour]
Quick-View   | ![Basalt Quick-View][Basalt Quick-View] | ![Diorite Quick-View][Diorite Quick-View]

<!-- Links / Images -->
[Basalt 24 Hour]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/basalt-24h.png?job=basalt "Basalt 24 Hour"
[Basalt 12 Hour]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/basalt-12h.png?job=basalt "Basalt 12 Hour"
[Basalt Quick-View]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/basalt-quick-view.png?job=basalt "Basalt Quick-View"

[Diorite 24 Hour]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/diorite-24h.png?job=diorite "Diorite 24 Hour"
[Diorite 12 Hour]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/diorite-12h.png?job=diorite "Diorite 12 Hour"
[Diorite Quick-View]: https://gitlab.com/adduc-projects/pebble-watchface-vitality-inspired/-/jobs/artifacts/master/raw/screenshots/diorite-quick-view.png?job=diorite "Diorite Quick-View"