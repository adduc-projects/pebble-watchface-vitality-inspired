#include <pebble.h>

static Layer *s_background_layer;

// Draw the horizontal background
static void prv_background_layer_update_callback(Layer *layer, GContext* ctx) {
  graphics_context_set_fill_color(ctx, GColorBlack);
  graphics_fill_rect(ctx, layer_get_bounds(layer), 0, GCornerNone);
}

static void background_load(Window *window) {
  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // The horizontal background
  GRect background_frame = GRect(0, 77, bounds.size.w, 77);
  s_background_layer = layer_create(background_frame);
  layer_set_update_proc(s_background_layer, prv_background_layer_update_callback);
  layer_add_child(window_layer, s_background_layer);
}

static void background_unload(Window *window) {
  layer_destroy(s_background_layer);
}
