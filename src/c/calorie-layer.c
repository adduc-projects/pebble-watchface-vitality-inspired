#include <pebble.h>
#include "positions.c"

static TextLayer *s_calorie_layer;

static void calorie_update(HealthEventType event) {
  HealthServiceAccessibilityMask steps = health_service_metric_accessible(
    HealthMetricActiveKCalories,
    time_start_of_today(),
    time(NULL)
  );

  if(steps & HealthServiceAccessibilityMaskAvailable) {
    // Data is available! Read it
    HealthValue kcalories = health_service_sum(HealthMetricActiveKCalories, time_start_of_today(), time(NULL));

    static char s_calorie_buffer[11];
    snprintf(s_calorie_buffer, sizeof(s_calorie_buffer), "%lu kcal", kcalories);
    text_layer_set_text(s_calorie_layer, s_calorie_buffer);
  }
}

static void calorie_load(Window *window) {

  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // Create time at top
  s_calorie_layer = text_layer_create(GRect(CALORIES_X, CALORIES_Y, CALORIES_WIDTH, CALORIES_HEIGHT));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_calorie_layer, GColorClear);
  text_layer_set_text_color(s_calorie_layer, GColorWhite);
  text_layer_set_font(s_calorie_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
  text_layer_set_text_alignment(s_calorie_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, text_layer_get_layer(s_calorie_layer));
}

static void calorie_unload(Window *window) {
  text_layer_destroy(s_calorie_layer);
}

static void calorie_init() {
  calorie_update(HealthEventMovementUpdate);
}
