#include <pebble.h>
#include "positions.c"

static BitmapLayer *s_bluetooth_layer;

static void bluetooth_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = GRect(BLUETOOTH_X, BLUETOOTH_Y, BLUETOOTH_WIDTH, BLUETOOTH_HEIGHT);

  static GBitmap *s_bitmap;
  s_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BLUETOOTH_IMAGE);
  s_bluetooth_layer = bitmap_layer_create(bounds);
  bitmap_layer_set_compositing_mode(s_bluetooth_layer, GCompOpSet);
  bitmap_layer_set_bitmap(s_bluetooth_layer, s_bitmap);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, bitmap_layer_get_layer(s_bluetooth_layer));
}