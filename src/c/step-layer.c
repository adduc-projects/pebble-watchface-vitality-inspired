#include <pebble.h>
#include "positions.c"

static TextLayer *s_step_layer;

static void step_update(HealthEventType event) {
  HealthServiceAccessibilityMask steps = health_service_metric_accessible(
    HealthMetricStepCount,
    time_start_of_today(),
    time(NULL)
  );

  if(steps & HealthServiceAccessibilityMaskAvailable) {
    // Data is available! Read it
    HealthValue steps = health_service_sum(HealthMetricStepCount, time_start_of_today(), time(NULL));

    static char s_step_buffer[11];
    snprintf(s_step_buffer, sizeof(s_step_buffer), "%lu st", steps);
    text_layer_set_text(s_step_layer, s_step_buffer);
  }
}

static void step_load(Window *window) {

  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // Create time at top
  s_step_layer = text_layer_create(GRect(STEP_X, STEP_Y, STEP_WIDTH, STEP_HEIGHT));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_step_layer, GColorClear);
  text_layer_set_text_color(s_step_layer, GColorWhite);
  text_layer_set_font(s_step_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
  text_layer_set_text_alignment(s_step_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, text_layer_get_layer(s_step_layer));
}

static void step_unload(Window *window) {
  text_layer_destroy(s_step_layer);
}

static void step_init() {
  step_update(HealthEventMovementUpdate);
}
