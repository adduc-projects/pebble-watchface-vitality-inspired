#include <pebble.h>
#include "positions.c"

static TextLayer *s_distance_layer;

static void distance_update(HealthEventType event) {
  HealthServiceAccessibilityMask steps = health_service_metric_accessible(
    HealthMetricWalkedDistanceMeters,
    time_start_of_today(),
    time(NULL)
  );

  if(steps & HealthServiceAccessibilityMaskAvailable) {
    // Data is available! Read it
    HealthValue meters = health_service_sum(
        HealthMetricWalkedDistanceMeters,
        time_start_of_today(),
        time(NULL)
    );

    static char s_distance_buffer[11];
    snprintf(s_distance_buffer, sizeof(s_distance_buffer), "%lu m", meters);
    text_layer_set_text(s_distance_layer, s_distance_buffer);
  }
}

static void distance_load(Window *window) {

  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // Create time at top
  s_distance_layer = text_layer_create(GRect(DISTANCE_X, DISTANCE_Y, DISTANCE_WIDTH, DISTANCE_HEIGHT));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_distance_layer, GColorClear);
  text_layer_set_text_color(s_distance_layer, GColorWhite);
  text_layer_set_font(s_distance_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
  text_layer_set_text_alignment(s_distance_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, text_layer_get_layer(s_distance_layer));
}

static void distance_unload(Window *window) {
  text_layer_destroy(s_distance_layer);
}

static void distance_init() {
  distance_update(HealthEventMovementUpdate);
}
