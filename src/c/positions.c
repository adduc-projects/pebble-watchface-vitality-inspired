#define TIME_X 0
#define TIME_Y -6
#define TIME_WIDTH 144
#define TIME_HEIGHT 57

#define DATE_X 0
#define DATE_Y 55
#define DATE_WIDTH 144
#define DATE_HEIGHT 57

#define STEP_X 0
#define STEP_Y 76
#define STEP_WIDTH 144.0 * .65
#define STEP_HEIGHT 27

#define DISTANCE_X 0
#define DISTANCE_Y 97
#define DISTANCE_WIDTH 144.0 * .65
#define DISTANCE_HEIGHT 27

#define CALORIES_X 0
#define CALORIES_Y 118
#define CALORIES_WIDTH 144.0 * .65
#define CALORIES_HEIGHT 27

#define GRAPH_X 0
#define GRAPH_Y 118
#define GRAPH_WIDTH 144
#define GRAPH_HEIGHT 24

#define BATTERY_X 144.0 * .65
#define BATTERY_Y 86
#define BATTERY_WIDTH 144.0 * .35
#define BATTERY_HEIGHT 24

#define BLUETOOTH_X 144.0 * .65
#define BLUETOOTH_Y 120
#define BLUETOOTH_WIDTH 144.0 * .35
#define BLUETOOTH_HEIGHT 24