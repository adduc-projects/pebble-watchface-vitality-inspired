#include <pebble.h>

#include "time-layer.c"
#include "background-layer.c"
#include "date-layer.c"
#include "step-layer.c"
#include "battery-layer.c"
#include "distance-layer.c"
#include "calorie-layer.c"
#include "bluetooth-layer.c"

static Window *s_main_window;

static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
  time_update(tick_time, units_changed);
  date_update(tick_time, units_changed);
}

static void battery_handler(BatteryChargeState charge) {
  battery_update(charge);
}

static void health_handler(HealthEventType event, void *context) {
  switch (event) {
    case HealthEventMovementUpdate:
      step_update(event);
      distance_update(event);
      calorie_update(event);
      break;
    default:
      break;
  }
}

static void main_window_load(Window *window) {
  time_load(window);
  background_load(window);
  date_load(window);
  step_load(window);
  battery_load(window);
  distance_load(window);
  calorie_load(window);
  bluetooth_load(window);
}

static void main_window_unload(Window *window) {
  time_unload(window);
  background_unload(window);
  date_unload(window);
  step_unload(window);
  battery_unload(window);
  distance_unload(window);
  calorie_unload(window);
}

static void init() {
  // Create main Window element and assign to pointer
  s_main_window = window_create();

  // Set handlers to manage the elements inside the Window
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload
  });

  // Show the Window on the watch, with animated=true
  window_stack_push(s_main_window, true);

  time_init();
  date_init();
  step_init();
  battery_init();
  distance_init();
  calorie_init();

  // Register with TickTimerService
  tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
  battery_state_service_subscribe(battery_handler);
  health_service_events_subscribe(health_handler, NULL);
}

static void deinit() {
  // Destroy Window
  window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
