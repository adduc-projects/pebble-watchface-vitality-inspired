#include <pebble.h>
#include "positions.c"

static TextLayer *s_date_layer;

static void date_update(struct tm *tick_time, TimeUnits units_changed) {
  if (units_changed & DAY_UNIT) {
    static char date_buffer[18];
    strftime(date_buffer, sizeof(date_buffer), "%A, %b %d", tick_time);
    text_layer_set_text(s_date_layer, date_buffer);
  }
}

static void date_load(Window *window) {
  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_date_layer = text_layer_create(GRect(DATE_X, DATE_Y, DATE_WIDTH, DATE_HEIGHT));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_date_layer, GColorClear);
  text_layer_set_text_color(s_date_layer, GColorBlack);
  text_layer_set_font(s_date_layer, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_CUYABRA_15)));
  text_layer_set_text_alignment(s_date_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, text_layer_get_layer(s_date_layer));
}

static void date_unload(Window *window) {
  text_layer_destroy(s_date_layer);
}

static void date_init() {
  // Get a tm structure
  time_t temp = time(NULL);
  struct tm *tick_time = localtime(&temp);

  // Make sure the time is displayed from the start
  date_update(tick_time, DAY_UNIT);
}
