#include <pebble.h>
#include "positions.c"

static Layer *s_battery_layer;

static void battery_update(BatteryChargeState charge) {
  layer_mark_dirty(s_battery_layer);
}

static void prv_battery_layer_update_callback(Layer *layer, GContext* ctx) {
  BatteryChargeState charge = battery_state_service_peek();

  GRect bounds = layer_get_bounds(layer);
  GRect circle_bounds = GRect(2, 2, bounds.size.w - 4, bounds.size.h - 4);

  graphics_context_set_stroke_color(ctx, GColorWhite);
  graphics_context_set_stroke_width(ctx, 2);
  graphics_draw_arc(ctx, circle_bounds, GOvalScaleModeFitCircle, 0, (charge.charge_percent / 100.0) * TRIG_MAX_ANGLE);

}

static void battery_load(Window *window)
{
  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = GRect(BATTERY_X, BATTERY_Y, BATTERY_WIDTH, BATTERY_HEIGHT);

  s_battery_layer = layer_create(bounds);
  layer_set_update_proc(s_battery_layer, prv_battery_layer_update_callback);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, s_battery_layer);
}

static void battery_unload(Window *window) {
  layer_destroy(s_battery_layer);
}

static void battery_init() {
}
