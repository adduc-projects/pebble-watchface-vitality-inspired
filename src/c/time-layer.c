#include <pebble.h>
#include "positions.c"

static TextLayer *s_time_layer;

static void time_update(struct tm *tick_time, TimeUnits units_changed) {
  // Write the current hours and minutes into a buffer
  static char time_buffer[8];

  if(clock_is_24h_style() == true) {
    // Use 24 hour format
    strftime(time_buffer, sizeof(time_buffer), "%H:%M", tick_time);
    text_layer_set_text(s_time_layer, time_buffer);
  } else {
    // Use 12 hour format
    strftime(time_buffer, sizeof(time_buffer), "%I:%M", tick_time);
    // strip leading zeroes
    text_layer_set_text(s_time_layer, time_buffer + (('0' == time_buffer[0]) ? 1 : 0));
  }
}

static void time_load(Window *window)
{
  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  // Create time at top
  s_time_layer = text_layer_create(GRect(TIME_X, TIME_Y, TIME_WIDTH, TIME_HEIGHT));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_time_layer, GColorClear);
  text_layer_set_text_color(s_time_layer, GColorBlack);
  text_layer_set_font(s_time_layer, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_55)));
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_layer, text_layer_get_layer(s_time_layer));
}

static void time_unload(Window *window) {
  text_layer_destroy(s_time_layer);
}

static void time_init() {
  // Get a tm structure
  time_t temp = time(NULL);
  struct tm *tick_time = localtime(&temp);

  // Make sure the time is displayed from the start
  time_update(tick_time, MINUTE_UNIT);
}
