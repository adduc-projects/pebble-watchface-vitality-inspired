build: _build

_build:
	pebble build

_build_emulator:
	pebble build -- --emulator

clean:
	pebble clean
	pebble wipe

# Hardware: https://developer.pebble.com/guides/tools-and-resources/hardware-information/

emu-classic: _build_emulator emu-aplite
emu-classic-steel: _build_emulator emu-aplite
emu-aplite: _build_emulator
	pebble install --emulator aplite

emu-time: _build_emulator emu-basalt
emu-time-steel: _build_emulator emu-basalt
emu-basalt: _build_emulator
	pebble install --emulator basalt

emu-time-round: _build_emulator emu-chalk
emu-chalk: _build_emulator
	pebble install --emulator chalk

emu-pebble-2: _build_emulator emu-diorite
emu-diorite: _build_emulator
	pebble install --emulator diorite

emu-time-2: _build_emulator emu-emery
emu-emery: _build_emulator
	pebble install --emulator emery

screenshot:
	pebble screenshot
